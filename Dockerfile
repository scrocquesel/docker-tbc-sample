FROM scratch

WORKDIR /

COPY Dockerfile .

LABEL test=test1 \
      org.opencontainers.image.revision=will-be-replaced
